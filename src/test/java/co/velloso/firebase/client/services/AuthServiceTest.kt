package co.velloso.firebase.client.services

import co.velloso.firebase.client.FirebaseApiClient
import co.velloso.firebase.client.MockData.API_KEY
import co.velloso.firebase.client.service.AuthService
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class AuthServiceTest {

    private val gson = GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").create()

    private val firebaseClient = Retrofit.Builder()
            .client(OkHttpClient.Builder().addInterceptor { chain ->
                val newRequest = chain.request().newBuilder()
                        .header("Content-Type", "application/json")
                        .build()
                chain.proceed(newRequest)
            }.build())
            .baseUrl("https://www.googleapis.com/identitytoolkit/v3/")
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
            .let { object : FirebaseApiClient(it) {} }

    @Before
    fun setUp() {
    }

    @Test
    fun `firebase sign up`() {
        // user must be able to get itself
        val signUnResponse = firebaseClient
                .auth()
                .signUp(API_KEY, AuthService.Request("jon@example.com", "12345678", true))
                .execute()
        println(signUnResponse.body()!!.refreshToken)
        println(signUnResponse.body()!!.idToken)
        Assert.assertNotNull(signUnResponse.body()!!.idToken.isNotBlank())
        Assert.assertTrue(signUnResponse.body()!!.refreshToken.isNotBlank())
    }

    @Test
    fun `firebase sign in`() {
        // user must be able to get itself
        val signInResponse = firebaseClient
                .auth()
                .signIn(API_KEY, AuthService.Request("gaia@example.com", "12345678", true))
                .execute()
        println(signInResponse.body()!!.refreshToken)
        println(signInResponse.body()!!.idToken)
        Assert.assertNotNull(signInResponse.body()!!.idToken.isNotBlank())
        Assert.assertTrue(signInResponse.body()!!.refreshToken.isNotBlank())
    }
}