package co.velloso.firebase.client.annotations

import java.lang.annotation.Inherited
import kotlin.reflect.KClass


@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@Inherited
annotation class Client(
        /**
         * The REST API version.
         *
         * @return The REST API version.
         */
        val version: String = "v1",

        /**
         * The search of endpoints included in the server.
         *
         * @return The search of services in the server.
         */
        val services: Array<KClass<*>>
)
