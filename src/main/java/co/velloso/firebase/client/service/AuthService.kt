package co.velloso.firebase.client.service

import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Query


interface AuthService {

    @POST("relyingparty/verifyPassword")
    fun signIn(
            @Query("key") key: String,
            @Body body: Request
    ): Call<Response>

    @POST("relyingparty/signupNewUser")
    fun signUp(
            @Query("key") key: String,
            @Body body: Request
    ): Call<Response>

    data class Request(
            var email: String = "",
            var password: String = "",
            var returnSecureToken: Boolean = false
    )

    data class Response(
            var kind: String = "",
            var localId: String = "",
            var email: String = "",
            var displayName: String = "",
            var idToken: String = "",
            var registered: Boolean = false,
            var refreshToken: String = "",
            var expiresIn: Int = 0
    )
}