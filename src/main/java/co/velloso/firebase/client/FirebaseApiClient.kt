package co.velloso.firebase.client

import co.velloso.firebase.client.annotations.Client
import co.velloso.firebase.client.service.AuthService
import retrofit2.Retrofit
import kotlin.reflect.KClass


@Client(
        version = "v1",
        services = [
            AuthService::class
        ]
)
abstract class FirebaseApiClient(retrofit: Retrofit) {

    private val services = mutableMapOf<KClass<*>, Any>()

    init {
        val annotation = this::class.java.annotations.find { it.annotationClass == Client::class } as? Client
        annotation?.services?.forEach {
            services[it] = retrofit.create(it.java)
        }
    }

    fun getService(className: KClass<*>) = services[className]

    fun auth() = getService(AuthService::class) as AuthService
}